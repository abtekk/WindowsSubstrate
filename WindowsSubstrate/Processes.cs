﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Diagnostics; //Needed for accessing Windows Processes.
using System.Runtime.InteropServices; //Needed for it's DLL Imports.

namespace WindowsSubstrate
{
    public partial class Substrate
    {
        //Get the passed process name and fetch its base address.
        public IntPtr GetBaseAddress(string NewProcessName)
        {
            Process[] processes = System.Diagnostics.Process.GetProcessesByName(NewProcessName);
            IntPtr baseAddr = processes[0].MainModule.BaseAddress;
            return baseAddr;
        }

        public Int64 GetProccessID(string NewProcessName)
        {
            Process[] processes = System.Diagnostics.Process.GetProcessesByName(NewProcessName);
            Int64 pID = processes[0].Id;
            return pID;
        }

        public string ReadMemory(string NewProcessName, int BytesRead, long Address)
        {
            int pID = (int)GetProccessID(NewProcessName);
            IntPtr BaseAddress = GetBaseAddress(NewProcessName);

            int bytes = 0;
            var buffer = new byte[BytesRead];
            var handle = OpenProcess(ProcessAccessFlags.All, false, pID);
            var memory = ReadProcessMemory((int)handle, Address, buffer, buffer.Length, ref bytes);
            string result = System.Text.Encoding.Unicode.GetString(buffer);
            return result;
        }

        public string WriteMemory(string NewProcessName, string ToWrite, long Address)
        {
            int pID = (int)GetProccessID(NewProcessName);
            IntPtr BaseAddress = GetBaseAddress(NewProcessName);

            int bytesWritten = 0;
            var buffer = Encoding.Unicode.GetBytes(ToWrite);
            var handle = (int)OpenProcess(ProcessAccessFlags.All, false, pID);
            bool memory = WriteProcessMemory(handle, Address, buffer, buffer.Length, ref bytesWritten);

            return Encoding.Unicode.GetString(buffer);
        }
    }
}
