﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Diagnostics; //Needed for accessing Windows Processes.
using System.Runtime.InteropServices; //Needed for it's DLL Imports.

namespace WindowsSubstrate
{
    public partial class Substrate
    {
        //Import the user32.dll at the correct function and make
        //the function public.
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern int SetWindowText(IntPtr hWnd, String text);
        //public static extern int WriteProcessMemory(IntPtr hProcess, String lpBaseAddress, String lpBuffer);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern int FindWindow(String ClassName, String WindowName);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        public static extern uint GetWindowThreadProcessId(int hWnd, IntPtr ProcessId);
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr OpenProcess(ProcessAccessFlags processAccess, bool bInheritHandle, int processId);
        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess, long lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);
        [DllImport("kernel32", SetLastError = true)]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, IntPtr bInheritHandle, IntPtr dwProcessId);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(int hProcess, long lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);
    }
}
