# Windows Substrate
Windows process and memory hacking framework developed in C#

# Installation
1. Clone the repository and compile the DLL.
2. Create a new C# console project and reference the WindowsSubstrate.dll.

# Features
1. Change the title of a window with the "ChangeWindowTitle" method
2. Get the base address of a process with "GetBaseAddress".
3. You can pass the process name, the amount of bytes to read and a memory offset to "Hack.ReadMemory", to read the data from that address, which will be returned back to the console.
4. In reflection of that, pass the process name, data you want to write and the memory offset to "Hack.WriteMemory", to overwite memory in that address.

# Notes
Expect rapid development. Eventually this will become a full hacking framework for Windows.
Please feel free to clone/send pull requests.

# Example Usage
>Change Window title.
```using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsSubstrate;

namespace SubstrateProcessPrinter
{
    class Program
    {
        static void Main(string[] args)
        {
            var Hack = new Substrate();
            Hack.ChangeWindowTitle("notepad", "This is my notepad!");
            Console.Read();
        }
    }
}
```





>Get the base address of a process in Int format. Run it over ToString("X") to get the hex value.

```using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsSubstrate;

namespace SubstrateProcessPrinter
{
    class Program
    {
        static void Main(string[] args)
        {
            var Hack = new Substrate();
            Int64 baseaddr = Hack.GetBaseAddress("Steam");
            Console.WriteLine(baseaddr);
            Console.Read();
        }
    }
}
```




> Read Process Memory (processname, bytestoread, memoryaddress)
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsSubstrate;

namespace SubstrateTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            var Hack = new Substrate();
            string memoryValue = Hack.ReadMemory("notepad", 24, 0x21392B77C70);
            Console.WriteLine(memoryValue);
            Console.Read();
        }
    }
}
```

> Write Process Memory (processname, "datatowrite", memoryaddress)
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsSubstrate;

namespace SubstrateTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            var Hack = new Substrate();
            string memoryValue = Hack.ReadMemory("notepad", "wasup", 0x21392B77C70);
            Console.WriteLine(memoryValue);
            Console.Read();
        }
    }
}
```
